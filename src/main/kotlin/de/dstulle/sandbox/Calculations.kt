package de.dstulle.sandbox

class Calculations {

    fun addition(addend1: Int, addend2: Int): Int {
        return addend1 + addend2
    }

    fun subtraction(minuend: Int, subtrahend: Int): Int {
        return minuend - subtrahend
    }

    fun multiplication(multiplier: Int, multiplicand: Int): Int {
        return multiplier * multiplicand
    }

    fun division(dividend: Int, divisor: Int): Pair<Int, Int> {
        return Pair(dividend / divisor, dividend % divisor)
    }

}