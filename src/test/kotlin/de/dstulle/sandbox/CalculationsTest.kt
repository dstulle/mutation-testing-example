package de.dstulle.sandbox

import de.dstulle.sandbox.Calculations
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class CalculationsTest {

    @Test
    fun addition() {
        val calculations = Calculations()

        assertEquals(0, calculations.addition(0,0))
        assertEquals(1, calculations.addition(1,0))
        assertEquals(1, calculations.addition(0,1))
        assertEquals(2, calculations.addition(1,1))
        assertEquals(20, calculations.addition(10,10))
        assertEquals(65, calculations.addition(23,42))
    }

    @Test
    fun subtraction() {
        val calculations = Calculations()

        assertEquals(0, calculations.subtraction(0,0))
        assertEquals(1, calculations.subtraction(1,0))
        assertEquals(-1, calculations.subtraction(0,1))
        assertEquals(0, calculations.subtraction(1,1))
        assertEquals(0, calculations.subtraction(10,10))
        assertEquals(-19, calculations.subtraction(23,42))
    }

    @Test
    fun multiplication() {
        val calculations = Calculations()

        assertEquals(0, calculations.multiplication(0,0))
        assertEquals(0, calculations.multiplication(1,0))
        assertEquals(0, calculations.multiplication(0,1))
        assertEquals(1, calculations.multiplication(1,1))
        assertEquals(100, calculations.multiplication(10,10))
        assertEquals(966, calculations.multiplication(23,42))
    }

    @Test
    fun division() {
        val calculations = Calculations()

        assertEquals(Pair(0, 0), calculations.division(0,1))
        assertEquals(Pair(1, 0), calculations.division(1,1))
        assertEquals(Pair(1, 0), calculations.division(10,10))
        assertEquals(Pair(3, 1), calculations.division(10,3))
        assertEquals(Pair(0, 5), calculations.division(5,10))
        assertEquals(Pair(0, 23), calculations.division(23,42))
        assertEquals(Pair(1, 19), calculations.division(42, 23))

//        assertEquals(Pair(0, 0), calculations.division(0,0))
//        assertEquals(Pair(0, 0), calculations.multiplication(1,0))

    }
}