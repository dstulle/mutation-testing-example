# Mutation Testing Example

This project demonstrates the use of mutation-testing with kotlin and gradle.

Mutation testing is used to evaluate the quality of existing automated test.
The main idea is to automatically change bits of the source code and check if the tests will fail.
If these mutations are spotted the tests work well.

## Usage

This Project contains a Simple class with four methods and an associated unit test.
The tests can be startet through gradle:

```bash
./gradlew test
```

The mutation test framework in use is [pitest](https://pitest.org/) and can also be started with gradle:

```bash
./gradlew pitest
```

All `test` and `pitest` runs should run fine out of the box.
But to see the `pitest` in action just remove assertions from the test and the tests will still be fine but the mutation test runs will start to find problems.

## License
Distributed under the MIT License. See [LICENSE]() for further information.
