import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.31"
    id("info.solidsoft.pitest") version "1.6.0"
    id("java-library")
    jacoco
}

group = "de.dstulle.sandbox"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
    pitest("org.pitest:pitest-junit5-plugin:0.12")
}

tasks.test {
    useJUnitPlatform()
    finalizedBy("jacocoTestReport")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.pitest {
    threads.set(4)
    timestampedReports.set(false)
    testPlugin.set("junit5")
}

jacoco {
    toolVersion = "0.8.6"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)

    val coverageSourceDirs = arrayOf(
        "src/main"
    )

    val classFiles = File("${buildDir}/classes/kotlin/")
        .walkBottomUp()
        .toSet()

    classDirectories.setFrom(classFiles)
    sourceDirectories.setFrom(files(coverageSourceDirs))

    executionData
        .setFrom(files("${buildDir}/jacoco/test.exec"))

    reports {
        xml.isEnabled = true
        html.isEnabled = true
    }
}